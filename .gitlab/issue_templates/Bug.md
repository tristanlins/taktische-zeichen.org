Wichtig: Falls du ein Problem mit den Taktischen Zeichen hast
(z.B. ein Zeichen ist falsch oder wird falsch dargestellt),
dann melde dies bitte direkt bei Jonas Köritz, dem Autor der Taktischen Zeichen!

--> https://github.com/jonas-koeritz/Taktische-Zeichen

-----

# Was wolltest du erreichen?

*Was war dein Ziel?*

# Wie war das Verhalten?

*Wie hat sich der Generator verhalten?*

# Was ist deine Erwartung?

*Wie sollte sich deiner Meinung nach der Generator verhalten?*

# Wie kann der Fehler reproduziert werden?

*Bitte Schritt für Schritt beschreiben, wie der Fehler reproduziert werden kann. Gerne auch mit Screenshots ergänzen.*

/assign @tristanlins
/label ~Bug
