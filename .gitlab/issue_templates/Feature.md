Wichtig: Falls du einen Verbesserungsvorschlag zu den Taktischen Zeichen hast
(z.B. ein neues Zeichen), dann melde dies bitte direkt bei Jonas Köritz, dem Autor der Taktischen Zeichen!

--> https://github.com/jonas-koeritz/Taktische-Zeichen

-----

# Was möchtest du erreichen?

*Was ist dein Ziel?*

# Wie soll dieses Verhalten erreicht werden?

*Wie stellst du dir eine konkrete Umsetzung vor?*

/assign @tristanlins
/label ~Feature
